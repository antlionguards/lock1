package com.gg.lock1;

import android.app.Application;

import com.gg.lock1.data.ObjectBox;

public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        ObjectBox.init(this);

    }

}
