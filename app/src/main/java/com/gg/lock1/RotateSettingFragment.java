package com.gg.lock1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.lock1.databinding.FragmentRotateSettingBinding;
import com.gg.lock1.other.Const;
import com.gg.lock1.other.Helper;

public class RotateSettingFragment extends Fragment {

    private FragmentRotateSettingBinding binding;
    private SettingViewModel viewModel;
    private SensorManager sensorManager;
    private Sensor accel;
    private Sensor mag;

    private final float[] accelerometerReading = new float[3];
    private final float[] magnetometerReading = new float[3];
    private final float[] rotationMatrix = new float[9];
    private final float[] orientationAngles = new float[3];

    private final SensorEventListener rotateListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                System.arraycopy(event.values, 0, accelerometerReading,
                        0, accelerometerReading.length);
            } else {
                if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                    System.arraycopy(event.values, 0, magnetometerReading,
                            0, magnetometerReading.length);
                }
            }

            boolean rotationOK = SensorManager.getRotationMatrix(rotationMatrix,
                    null, accelerometerReading, magnetometerReading);
            if (rotationOK) {
                SensorManager.getOrientation(rotationMatrix, orientationAngles);
            }
            int pitchAngle = (int) Math.toDegrees(orientationAngles[1]);
            String text;
            if (pitchAngle < 0) {
                text = "Above: " + Math.abs(pitchAngle) + " degree";
            } else {
                text = "Below: " + Math.abs(pitchAngle) + " degree";
            }
            binding.value.setText(text);

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public RotateSettingFragment() {
    }

    public static RotateSettingFragment newInstance() {
        RotateSettingFragment fragment = new RotateSettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = getActivity();
        if (activity != null) {
            sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
            accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mag = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        }
        viewModel = new ViewModelProvider(this).get(SettingViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_rotate_setting, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (sensorManager != null && mag != null && accel != null) {
            sensorManager.registerListener(rotateListener, mag, SensorManager.SENSOR_DELAY_NORMAL, 0);
            sensorManager.registerListener(rotateListener, accel, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }

        int currentMode = viewModel.getRotateUnlockType();
        if (currentMode == Const.ROTATE_UNLOCK_UP) {
            binding.pullUp1.setChecked(true);
        } else {
            if (currentMode == Const.ROTATE_UNLOCK_DOWN) {
                binding.pullDown1.setChecked(true);
            }
        }

        binding.saveBtn.setOnClickListener(v -> {
            int type = -1;
            if (binding.pullUp1.isChecked()) {
                type = Const.ROTATE_UNLOCK_UP;
            } else {
                if (binding.pullDown1.isChecked()) {
                    type = Const.ROTATE_UNLOCK_DOWN;
                }
            }
            if (type != -1) {
                if (viewModel.saveRotateSetting(type)) {
                    Helper.showToast("success", getContext());
                    Activity activity = getActivity();
                    if (activity instanceof MainActivity) {
                        ((MainActivity) activity).notifyServiceUnlockMethodChanged();
                    }
                } else {
                    Helper.showToast("failed", getContext());
                }
            }
        });
    }
}