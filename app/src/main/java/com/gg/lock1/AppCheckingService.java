package com.gg.lock1;

import static com.gg.lock1.MainActivity.TAG;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.PixelFormat;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Messenger;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.core.app.NotificationCompat;

import com.gg.lock1.data.BlockedAppInfo;
import com.gg.lock1.data.ObjectBox;
import com.gg.lock1.other.Const;
import com.gg.lock1.other.Helper;
import com.gg.lock1.other.LockMethod;

import java.util.ArrayList;
import java.util.List;

public class AppCheckingService extends Service {

    public static boolean isRunning = false;

    private final int LOCK_SERVICE_NOTIFICATION_ID = 123;
    private final String CHANNEL_ID = "lock1.notification";
    private final String CHANNEL_NAME = "LockTest";
    private final long DEFAULT_DELAY = 60;
    //private final long DEFAULT_IGNORE_CHECK_INTERVAL = 50;

    //private ActivityManager activityManager;
    private UsageStatsManager usageManager;
    private PackageManager packageManager;
    private SensorManager sensorManager;
    private SharedPreferences preferences;

    private final HandlerThread handlerThread = new HandlerThread("AppCheckingThread");
    private Handler appCheckingHandler;
    private final Runnable appChecking = this::getRunningApp;

    List<BlockedAppInfo> blockedApps = new ArrayList<>();
    String defaultLauncherPkg = "";
    private String lastForegroundPackage = "";

    private View overlayView;
    private View contentRoot;
    private WindowManager.LayoutParams params;
    private boolean isLockViewAdded = false;
    private Animation appearAnimation;
    private Animation hideAnimation;
    private boolean isHideAnimationRunning = false;
    private final Animation.AnimationListener hideAnimListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
            isHideAnimationRunning = true;
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            Log.d(TAG, "on hide Animation End: ");
            isHideAnimationRunning = false;
            if (contentRoot != null) {
                contentRoot.setVisibility(View.INVISIBLE);
            }
            removeLockView();
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    private Messenger messenger;
    private final Handler msgHandler = new Handler(msg -> {
        Log.d(TAG, "AppCheckingService got a msg from msg handler ");
        switch (msg.what) {
            case Const.MSG_BLOCKED_LIST_CHANGED: {
                getBlockedApps();
                return true;
            }

            case Const.MSG_UNLOCK_METHOD_CHANGED: {
                getLockMethods();
                return true;
            }
        }
        return false;
    });

    private boolean isLightMethodEnable = false;
    private boolean isMagMethodEnable = false;
    private boolean isProMethodEnable = false;
    private boolean isRotateMethodEnable = false;

    private Sensor lightSen;
    private Sensor magSen;
    private Sensor proSen;
    private Sensor accelSen;

    private int lightValueStart = 0;
    private int lightValueEnd = 0;
    private int lightTime = 0;//in millis
    private long lightTimeCorrectStart = 0;//time in millis that the light value start change from lock to unlock value
    private long lightTimeCorrectCount = 0;//duration of correct light value (current time -  lightTimeCorrectStart)
    private final SensorEventListener lightListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.values != null && event.values.length > 0) {
                checkUnlockByLight1(event.values[0]);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private int magValueStart = 0;
    private int magValueEnd = 0;
    private int magTime = 0;//in millis
    private long magTimeCorrectStart = 0;//time in millis that the mag value start change from lock to unlock value
    private long magTimeCorrectCount = 0;//duration of correct mag value (current time -  magTimeCorrectStart)
    private final SensorEventListener magListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.values != null && event.values.length >= 3) {
                checkUnlockByMag(Math.sqrt(event.values[0] * event.values[0]
                        + event.values[1] * event.values[1]
                        + event.values[2] * event.values[2]));

                System.arraycopy(event.values, 0, magnetometerReading,
                        0, magnetometerReading.length);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private int proUncoverCount = -1;//start = uncover
    private int proCoverCount = 0;
    private long lastProDetectTime = 0;
    private int proDetectType = -1;
    private int proSettingCoverCount = 0;
    private int proSettingKeepTime = 0;
    private final Runnable checkCoverRunnable = this::checkCoverToUnlock;

    private final SensorEventListener proListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.values != null && event.values.length > 0) {
                if (event.values[0] < event.sensor.getMaximumRange()) {
                    proSenCovered();
                } else {
                    proSenUncover();
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    private final float[] accelerometerReading = new float[3];
    private final float[] magnetometerReading = new float[3];
    private final float[] rotationMatrix = new float[9];
    private final float[] orientationAngles = new float[3];
    private int rotateType = -1;
    private int lastPitchAngle = 0;
    private boolean isFirstPointReached = false;

    private final SensorEventListener rotateListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                System.arraycopy(event.values, 0, accelerometerReading,
                        0, accelerometerReading.length);
            } else {
                if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
                    System.arraycopy(event.values, 0, magnetometerReading,
                            0, magnetometerReading.length);
                }
            }

            boolean rotationOK = SensorManager.getRotationMatrix(rotationMatrix,
                    null, accelerometerReading, magnetometerReading);
            if (rotationOK) {
                SensorManager.getOrientation(rotationMatrix, orientationAngles);
            }
            int pitchAngle = (int) Math.toDegrees(orientationAngles[1]);
            if (rotateType == Const.ROTATE_UNLOCK_UP) {
                checkUnlockByRotateUp(pitchAngle);
            } else {
                //if (rotateType == Const.ROTATE_UNLOCK_DOWN) {
                checkUnlockByRotateDown(pitchAngle);
                // }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    @Override
    public void onCreate() {
        //activityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            usageManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            //todo
        }
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        packageManager = getPackageManager();
        preferences = Helper.getSettingPref(this);
        getLauncherApp1();
        getLockMethods();

        appearAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_up_1);
        hideAnimation = AnimationUtils.loadAnimation(this, R.anim.scale_down_1);
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        isRunning = true;
        startForeground(LOCK_SERVICE_NOTIFICATION_ID, createNotification());

        if (!handlerThread.isAlive()) {
            handlerThread.start();
            appCheckingHandler = new Handler(handlerThread.getLooper());
            appCheckingHandler.postDelayed(this::getRunningApp, 500);
            getBlockedApps();
        }
        return START_STICKY;
    }

    public AppCheckingService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        messenger = new Messenger(msgHandler);
        return messenger.getBinder();
    }

    private Notification createNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription("Test Description");

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(channel);
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setContentTitle("Test Title")
                .setContentText("Test text")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);
        return builder.build();

    }

    void getBlockedApps() {
        appCheckingHandler.post(() -> {
            Log.d(TAG, "AppCheckingService getBlockedApps: ");
            blockedApps = ObjectBox.get()
                    .boxFor(BlockedAppInfo.class)
                    .getAll();
        });
    }

    void getLauncherApp1() {
        Intent filter = new Intent(Intent.ACTION_MAIN);
        filter.addCategory(Intent.CATEGORY_HOME);
        List<ResolveInfo> infoList = packageManager.queryIntentActivities(filter, PackageManager.MATCH_DEFAULT_ONLY);
        //info list may contains default setting activity
        if (infoList.size() > 1) {
            for (ResolveInfo info : infoList) {
                // Log.d(TAG, "getLauncherApp: " + info.activityInfo.packageName);
                if (info.activityInfo.packageName.contains("launcher")) {
                    defaultLauncherPkg = info.activityInfo.packageName;
                    break;
                }
            }
        }
    }

    void getRunningApp() {

        UsageEvents events = usageManager.queryEvents(System.currentTimeMillis() - 60000, System.currentTimeMillis());
        UsageEvents.Event event = new UsageEvents.Event();
        long eventTime = 0;
        String mostRecent = "";

        while (events.hasNextEvent()) {
            events.getNextEvent(event);
            if (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND) {
                if (event.getTimeStamp() > eventTime) {
                    mostRecent = event.getPackageName();
                    eventTime = event.getTimeStamp();
                }
            }
        }
        if (!mostRecent.isEmpty()) {
            if (!mostRecent.equals(lastForegroundPackage)) {

                if (mostRecent.equals(defaultLauncherPkg)) {
                    Log.d(TAG, "press home/recent: ");
                    hideLockView();
                } else {
                    Log.d(TAG, "open an app");
                    checkBlockApp(mostRecent);
                }
            }
            lastForegroundPackage = mostRecent;
        }

        if (appCheckingHandler != null) {
            appCheckingHandler.postDelayed(appChecking, DEFAULT_DELAY);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
        if (appCheckingHandler != null) {
            appCheckingHandler.removeCallbacks(appChecking);
            appCheckingHandler.removeCallbacks(checkCoverRunnable);
        }
        removeLockView();
        handlerThread.quitSafely();
    }

    private void createOverlayView() {
        LayoutInflater inflater = LayoutInflater.from(this);
        overlayView = inflater.inflate(R.layout.layout_lock_overlay, null);
        contentRoot = overlayView.findViewById(R.id.lockOverlayContentRoot);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.RGBA_8888);
        } else {
            params = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    PixelFormat.RGBA_8888);
        }
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.MATCH_PARENT;
    }

    private void showLockView() {
        if (isLockViewAdded) return;
        if (overlayView == null) createOverlayView();
        Log.d(TAG, "showLockView:");
        WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        manager.addView(overlayView, params);
        initSensorListener();

        hideAnimation.setAnimationListener(null);
        contentRoot.setVisibility(View.VISIBLE);
        contentRoot.startAnimation(appearAnimation);
        isLockViewAdded = true;
    }

    private void hideLockView() {
        if (isLockViewAdded && !isHideAnimationRunning) {
            cancelSensorListener();
            Log.d(TAG, "hideLockView:");
            overlayView.post(() -> {
                hideAnimation.setAnimationListener(hideAnimListener);
                contentRoot.clearAnimation();
                contentRoot.startAnimation(hideAnimation);
            });
        }
    }

    private void removeLockView() {
        Log.d(TAG, "removeLockView:");
        if (contentRoot != null) {
            contentRoot.clearAnimation();
        }
        hideAnimation.setAnimationListener(null);

        if (isLockViewAdded) {
            WindowManager manager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
            manager.removeView(overlayView);
            isLockViewAdded = false;
        }
    }

    private void checkBlockApp(String lastUsedPackageName) {
        if (lastUsedPackageName == null) return;

        for (BlockedAppInfo blocked : blockedApps) {
            if (lastUsedPackageName.equals(blocked.getPackageName())) {
                showLockView();
                return;
            }
        }

        //hideLockView();
    }


    private void initSensorListener() {
        resetValues();
        if (lightSen == null && isLightMethodEnable) {
            lightSen = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
            sensorManager.registerListener(lightListener, lightSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }
        if (magSen == null && isMagMethodEnable) {
            magSen = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            sensorManager.registerListener(magListener, magSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }
        if (proSen == null && isProMethodEnable) {
            proSen = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
            sensorManager.registerListener(proListener, proSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }
        if (isRotateMethodEnable) {
            if (magSen == null) {
                magSen = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            }
            if (accelSen == null) {
                accelSen = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            }
            sensorManager.registerListener(rotateListener, accelSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
            sensorManager.registerListener(rotateListener, magSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }
    }

    private void cancelSensorListener() {
        resetValues();
        if (lightSen != null) {
            sensorManager.unregisterListener(lightListener, lightSen);
            lightSen = null;
        }
        if (magSen != null) {
            sensorManager.unregisterListener(magListener, magSen);
            sensorManager.unregisterListener(rotateListener, magSen);
            magSen = null;
        }
        if (proSen != null) {
            sensorManager.unregisterListener(magListener, proSen);
            proSen = null;
        }
        if (accelSen != null) {
            sensorManager.unregisterListener(rotateListener, accelSen);
            accelSen = null;
        }
    }

    private void resetValues() {
        lightTimeCorrectStart = 0;
        lightTimeCorrectCount = 0;

        magTimeCorrectStart = 0;
        magTimeCorrectCount = 0;

        proCoverCount = 0;
        proUncoverCount = 0;

        isFirstPointReached = false;
        lastPitchAngle = 0;
    }


    private void getLockMethods() {
        isLightMethodEnable = preferences.getBoolean(LockMethod.light.value, false);
        isMagMethodEnable = preferences.getBoolean(LockMethod.magnetic.value, false);
        isProMethodEnable = preferences.getBoolean(LockMethod.pro.value, false);
        isRotateMethodEnable = preferences.getBoolean(LockMethod.rotate.value, false);

        getUnlockSetting();
    }

    private void getUnlockSetting() {
        lightValueStart = preferences.getInt(Const.LIGHT_UNLOCK_VALUE_START, 0);
        lightValueEnd = preferences.getInt(Const.LIGHT_UNLOCK_VALUE_END, 0);
        lightTime = preferences.getInt(Const.LIGHT_UNLOCK_TIME, 0) * 1000;

        magValueStart = preferences.getInt(Const.MAG_UNLOCK_VALUE_START, 0);
        magValueEnd = preferences.getInt(Const.MAG_UNLOCK_VALUE_END, 0);
        magTime = preferences.getInt(Const.MAG_UNLOCK_TIME, 0) * 1000;

        proDetectType = preferences.getInt(Const.PRO_UNLOCK_TYPE, -1);
        proSettingKeepTime = preferences.getInt(Const.PRO_UNLOCK_KEEP_TIME, 0);
        proSettingCoverCount = preferences.getInt(Const.PRO_UNLOCK_COVER_COUNT, 0);

        rotateType = preferences.getInt(Const.ROTATE_UNLOCK_TYPE, -1);
        resetValues();
        Log.d(TAG, "getUnlockSetting: ");
    }

    private void checkUnlockByLight1(float value) {
        boolean isValueCorrect = ((value >= lightValueStart && value <= lightValueEnd)
                || (value >= lightValueEnd && value <= lightValueStart));
        if (lightTime < 1) {
            if (isValueCorrect) {
                Log.d(TAG, "hide from light value < 1");
                hideLockView();
            }
        } else {
            long currentTime = System.currentTimeMillis();
            if (isValueCorrect) {
                if (lightTimeCorrectStart == 0) {
                    lightTimeCorrectStart = currentTime;
                }
                lightTimeCorrectCount = currentTime - lightTimeCorrectStart;
            } else {
                lightTimeCorrectStart = 0;
                lightTimeCorrectCount = 0;
            }
            if (lightTimeCorrectCount >= lightTime) {
                Log.d(TAG, "hide value from light >= 1: " + lightTimeCorrectCount + "/" + lightTime);
                hideLockView();
            }
        }
    }

    private void checkUnlockByMag(double value) {
        boolean isValueCorrect = ((value >= magValueStart && value <= magValueEnd)
                || (value >= magValueEnd && value <= magValueStart));
        if (magTime < 1) {
            if (isValueCorrect) {
                Log.d(TAG, "hide from mag value < 1");
                hideLockView();
            }
        } else {
            long currentTime = System.currentTimeMillis();
            if (isValueCorrect) {
                if (magTimeCorrectStart == 0) {
                    magTimeCorrectStart = currentTime;
                }
                magTimeCorrectCount = currentTime - magTimeCorrectStart;
            } else {
                magTimeCorrectStart = 0;
                magTimeCorrectCount = 0;
            }
            if (magTimeCorrectCount >= magTime) {
                Log.d(TAG, "hide value from mag >= 1: " + magTimeCorrectCount + "/" + magTime);
                hideLockView();
            }
        }
    }

    private void proSenCovered() {
        if (proDetectType == Const.PRO_UNLOCK_COVER) {
            proCoverCount++;
            Log.d(TAG, "proSenCovered: " + proCoverCount);
            return;
        }
        if (proDetectType == Const.PRO_UNLOCK_KEEP) {
            lastProDetectTime = System.currentTimeMillis();
            Log.d(TAG, "proSenCovered: post for " + proSettingKeepTime * 1000L);
            appCheckingHandler.postDelayed(checkCoverRunnable, proSettingKeepTime * 1000L);
        }
    }

    private void checkCoverToUnlock() {
        //check again
        Log.d(TAG, "checkCoverToUnlock");
        if (lastProDetectTime != 0
                && (System.currentTimeMillis() - lastProDetectTime) >= (proSettingKeepTime * 1000L)) {
            hideLockView();
        }
    }

    private void proSenUncover() {
        if (proDetectType == Const.PRO_UNLOCK_COVER) {
            proUncoverCount++;
            Log.d(TAG, "cover / un =  " + proCoverCount + "/" + proUncoverCount);
            if (proUncoverCount >= proSettingCoverCount
                    && proCoverCount >= proSettingCoverCount) {
                hideLockView();
            }
            return;
        }
        if (proDetectType == Const.PRO_UNLOCK_KEEP) {
            lastProDetectTime = 0;
            appCheckingHandler.removeCallbacks(checkCoverRunnable);
        }
    }

    private void checkUnlockByRotateUp(int angle) {
        if (lastPitchAngle == 0) {
            lastPitchAngle = angle;
            return;
        }
        if (angle > 0) {
            lastPitchAngle = 0;
            return;
        }
        int changeAmount = angle - lastPitchAngle;
        if (changeAmount > 5) {
            //pull down
            lastPitchAngle = angle;
            isFirstPointReached = false;
            return;
        }
        if (Math.abs(changeAmount) < 10) {
            return;
        }
        if (angle < -30 && lastPitchAngle > -30) {
            isFirstPointReached = true;
        }
        if (isFirstPointReached && angle < -70) {
            hideLockView();
        }
        lastPitchAngle = angle;
        Log.d(TAG, "checkUnlockByRotateUp: " + angle);
    }

    private void checkUnlockByRotateDown(int angle) {
        if (angle > 0) {
            lastPitchAngle = 0;
            return;
        }
        if (lastPitchAngle == 0) {
            lastPitchAngle = angle;
            return;
        }
        int changeAmount = angle - lastPitchAngle;
        if (changeAmount < -5) {
            //pull up
            isFirstPointReached = false;
            lastPitchAngle = angle;
            return;
        }
        if (Math.abs(changeAmount) < 10) {
            return;
        }
        if (angle > -70 && lastPitchAngle <= -70) {
            isFirstPointReached = true;
        }
        if (isFirstPointReached && angle > -30) {
            hideLockView();
        }
        lastPitchAngle = angle;
        Log.d(TAG, "checkUnlockByRotateDown: " + angle);
    }


}