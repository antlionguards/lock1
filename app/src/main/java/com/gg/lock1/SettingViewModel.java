package com.gg.lock1;

import android.app.Application;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.gg.lock1.other.Const;
import com.gg.lock1.other.Helper;

public class SettingViewModel extends AndroidViewModel {
    private final SharedPreferences preferences;

    public SettingViewModel(@NonNull Application application) {
        super(application);
        preferences = Helper.getSettingPref(application);
    }

    public boolean getLockModeStatus(String name) {
        return Helper.getPrefBoolean(preferences, name);
    }

    public int getStatusString(boolean isAvailable) {
        if (isAvailable) {
            return R.string.available;
        } else {
            return R.string.unavailable;
        }
    }

    /**
     * @param name -> lock mode name
     * @return -> current lock mode status
     */
    public boolean toggleLockMode(String name) {
        boolean isEnabled = Helper.getPrefBoolean(preferences, name);
        if (Helper.putPrefBoolean(preferences, name, !isEnabled)) {
            return !isEnabled;
        }
        return isEnabled;
    }

    public boolean saveLightSetting(int unlockValueStart, int unlockValueEnd, int time) {
        SharedPreferences.Editor editor = preferences.edit();
        return editor
                .putInt(Const.LIGHT_UNLOCK_VALUE_START, unlockValueStart)
                .putInt(Const.LIGHT_UNLOCK_VALUE_END, unlockValueEnd)
                .putInt(Const.LIGHT_UNLOCK_TIME, time)
                .commit();
    }

    public int getLightValueStart() {
        return preferences.getInt(Const.LIGHT_UNLOCK_VALUE_START, 0);
    }

    public int getLightValueEnd() {
        return preferences.getInt(Const.LIGHT_UNLOCK_VALUE_END, 0);
    }

    public int getLightTime() {
        return preferences.getInt(Const.LIGHT_UNLOCK_TIME, 0);
    }

    public boolean saveMagSetting(int unlockValueStart, int unlockValueEnd, int time) {
        SharedPreferences.Editor editor = preferences.edit();
        return editor
                .putInt(Const.MAG_UNLOCK_VALUE_START, unlockValueStart)
                .putInt(Const.MAG_UNLOCK_VALUE_END, unlockValueEnd)
                .putInt(Const.MAG_UNLOCK_TIME, time)
                .commit();
    }

    public int getMagValueStart() {
        return preferences.getInt(Const.MAG_UNLOCK_VALUE_START, 0);
    }

    public int getMagValueEnd() {
        return preferences.getInt(Const.MAG_UNLOCK_VALUE_END, 0);
    }

    public int getMagTime() {
        return preferences.getInt(Const.MAG_UNLOCK_TIME, 0);
    }

    public int getProType() {
        return preferences.getInt(Const.PRO_UNLOCK_TYPE, -1);
    }

    public int getProCoverCount() {
        return preferences.getInt(Const.PRO_UNLOCK_COVER_COUNT, 1);
    }

    public int getProKeepTime() {
        return preferences.getInt(Const.PRO_UNLOCK_KEEP_TIME, 1);
    }

    public boolean saveProSettingCover(int count) {
        SharedPreferences.Editor editor = preferences.edit();
        return editor
                .putInt(Const.PRO_UNLOCK_TYPE, Const.PRO_UNLOCK_COVER)
                .putInt(Const.PRO_UNLOCK_COVER_COUNT, count)
                .commit();
    }

    public boolean saveProSettingKeep(int time) {
        SharedPreferences.Editor editor = preferences.edit();
        return editor
                .putInt(Const.PRO_UNLOCK_TYPE, Const.PRO_UNLOCK_KEEP)
                .putInt(Const.PRO_UNLOCK_KEEP_TIME, time)
                .commit();
    }

    public boolean saveRotateSetting(int type){
        SharedPreferences.Editor editor = preferences.edit();
        return editor
                .putInt(Const.ROTATE_UNLOCK_TYPE, type)
                .commit();
    }

    public int getRotateUnlockType(){
        return preferences.getInt(Const.ROTATE_UNLOCK_TYPE, -1);
    }
}
