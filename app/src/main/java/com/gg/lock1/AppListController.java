package com.gg.lock1;

import static com.gg.lock1.MainActivity.TAG;

import android.util.Log;
import android.view.View;
import com.airbnb.epoxy.Typed2EpoxyController;
import com.gg.lock1.data.ShortAppInfo;
import com.gg.lock1.other.ClickCallback;

import java.util.List;

public class AppListController extends Typed2EpoxyController<List<ShortAppInfo>, ClickCallback> {

    private List<ShortAppInfo> apps;


    @Override
    public void setData(List<ShortAppInfo> data1, ClickCallback data2) {
        super.setData(data1, data2);
        apps = data1;
    }

    public void updateItem(ShortAppInfo appInfo) {
        if (appInfo == null) return;
        CharSequence updatedPackageName = appInfo.getPackageName();
        if (updatedPackageName == null) return;
        for (int i = 0; i < apps.size(); i++) {
            if (updatedPackageName.equals(apps.get(i).getPackageName())){
                apps.set(i, appInfo);
                notifyModelChanged(i);
                break;
            }
        }
    }

    @Override
    protected void buildModels(List<ShortAppInfo> data, ClickCallback callback) {
        for (ShortAppInfo app : data) {
            add(new ItemAppBindingModel_()
                    .id(app.getPackageName())
                    .clickListener(v -> {
                        if (callback != null) {
                            callback.onCheckedChange(app, !app.isBlocked());
                        }
                    })
                    /*
                    .checkedChange((buttonView, isChecked) -> {
                        Log.d(TAG, "checkedChange: " + isChecked);
                        if (callback != null && isChecked != app.isBlocked()) {
                            callback.onCheckedChange(app, isChecked);
                        }
                    })
                    */
                    .appInfo(app));


            Log.d(TAG, "buildModels: " + app.getName() + "/" + app.getPackageName()
                    + "/ icon size = " + app.getBitmap().getByteCount() / 1024);
        }
    }

}
