package com.gg.lock1;

import android.app.Activity;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import static com.gg.lock1.MainActivity.TAG;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.lock1.data.ShortAppInfo;
import com.gg.lock1.databinding.FragmentHomeBinding;
import com.gg.lock1.other.ClickCallback;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    private FragmentHomeBinding binding;

    private AppInfoViewModel viewModel;

    private AppListController controller;

    private final String KEY_SERVICE_CONNECT_STATUS = "key_service_connect_status";
    private boolean isServiceConnected = false;

    private final ClickCallback appClickCallback = new ClickCallback() {
        @Override
        public void onCheckedChange(ShortAppInfo appInfo, boolean isChecked) {
            handleAppCheck(appInfo, isChecked);
        }
    };

    private final Observer<List<ShortAppInfo>> appInfoListObserver = shortAppInfoList -> {
        hideLoading();
        if (controller != null && shortAppInfoList != null) {
            controller.setData(shortAppInfoList, appClickCallback);
        }
    };

    public HomeFragment() {
    }

    public static HomeFragment newInstance(boolean isServiceConnected) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putBoolean(fragment.KEY_SERVICE_CONNECT_STATUS, isServiceConnected);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args;
        if (savedInstanceState != null) {
            args = savedInstanceState;
        } else {
            args = getArguments();
        }
        if (args != null) {
            isServiceConnected = args.getBoolean(KEY_SERVICE_CONNECT_STATUS, false);
        }
        viewModel = new ViewModelProvider(this).get(AppInfoViewModel.class);
        observe();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(KEY_SERVICE_CONNECT_STATUS, isServiceConnected);
        super.onSaveInstanceState(outState);
    }

    private void observe() {
        viewModel.getAppInfoListLiveData().observe(this, appInfoListObserver);
        viewModel.getLoadingLive().observe(this, isLoading -> {
            if (isLoading) {
                Log.d(TAG, "show loading from live: ");
                showLoading();
            } else {
                Log.d(TAG, "hide loading from live: ");
                hideLoading();
            }
        });
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnOn.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).startAppCheckingService();
            }
        });

        binding.btnOff.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).stopAppCheckingService();
            }
        });

        binding.btnSetting.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).addFragment(new SettingFragment());
            }
        });
        handleServiceState(isServiceConnected);

        controller = new AppListController();
        binding.appRcv.setController(controller);

    }

    public void showLoading() {
        Log.d(TAG, "showLoading: from home fragment");
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).showProgress();
        }
    }

    public void hideLoading() {
        Log.d(TAG, "hideLoading: from home fragment");
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).hideProgress();
        }
    }

    @Override
    public void onDetach() {
        Log.d(TAG, "getView before super onDetach: " + getView());
        super.onDetach();
        Log.d(TAG, "getView after super onDetach: " + getView());
    }

    @Override
    public void onDestroyView() {
        Log.d(TAG, "getView before super onDestroyView: " + getView());
        super.onDestroyView();
        Log.d(TAG, "getView after super onDestroyView: " + getView());
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "getView before super onDestroy: " + getView());
        super.onDestroy();
        Log.d(TAG, "getView after super onDestroy: " + getView());
    }

    private void handleAppCheck(ShortAppInfo appInfo, boolean isChecked) {
        if (isChecked) {
            viewModel.blockApp(appInfo).observe(this, isBlocked -> {
                hideLoading();
                appInfo.setBlocked(isBlocked);
                controller.updateItem(appInfo);
                notifyServiceToUpdateBlockedApps();
            });
        } else {
            viewModel.removeBlockApp(appInfo).observe(this, isUnblocked -> {
                hideLoading();
                appInfo.setBlocked(!isUnblocked);
                controller.updateItem(appInfo);
                notifyServiceToUpdateBlockedApps();
            });
        }
    }

    private void notifyServiceToUpdateBlockedApps() {
        if (!isServiceConnected) return;
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).notifyServiceToGetBlockedApps();
        }
    }

    public void handleServiceState(boolean isConnected) {
        isServiceConnected = isConnected;
        if (isConnected) {
            binding.btnOn.setVisibility(View.GONE);
            binding.btnOff.setVisibility(View.VISIBLE);
            viewModel.getInstalledApps();
        } else {
            binding.btnOn.setVisibility(View.VISIBLE);
            binding.btnOff.setVisibility(View.GONE);
            if (controller != null) {
                controller.setData(new ArrayList<>(), appClickCallback);
            }
        }
    }
}