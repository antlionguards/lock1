package com.gg.lock1;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gg.lock1.data.AppInfoRepo;
import com.gg.lock1.data.ShortAppInfo;

import java.util.List;

import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;

public class AppInfoViewModel extends AndroidViewModel {
    private final AppInfoRepo repo;

    private final MutableLiveData<List<ShortAppInfo>> appInfoListLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loadingLive = new MutableLiveData<>();

    public LiveData<List<ShortAppInfo>> getAppInfoListLiveData() {
        return appInfoListLiveData;
    }

    public LiveData<Boolean> getLoadingLive() {
        return loadingLive;
    }

    public AppInfoViewModel(@NonNull Application application) {
        super(application);
        repo = new AppInfoRepo();
    }

    public void getInstalledApps() {
        loadingLive.setValue(true);
        repo.getInstalledApps(getApplication().getPackageManager())
                .subscribe(new SingleObserver<List<ShortAppInfo>>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull List<ShortAppInfo> shortAppInfos) {
                        loadingLive.setValue(false);
                        appInfoListLiveData.setValue(shortAppInfos);
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        loadingLive.setValue(false);
                    }
                });
    }

    public LiveData<Boolean> blockApp(ShortAppInfo appInfo) {
        loadingLive.setValue(true);
        return repo.blockApp(appInfo);
    }

    public LiveData<Boolean> removeBlockApp(ShortAppInfo appInfo) {
        loadingLive.setValue(true);
        return repo.removeBlockApp(appInfo);
    }
}
