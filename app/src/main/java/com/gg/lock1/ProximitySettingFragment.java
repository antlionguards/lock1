package com.gg.lock1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.lock1.databinding.FragmentProximitySettingBinding;
import com.gg.lock1.other.Const;
import com.gg.lock1.other.Helper;

public class ProximitySettingFragment extends Fragment {

    private FragmentProximitySettingBinding binding;
    private Sensor proSen;
    private SensorManager sensorManager;
    private SettingViewModel viewModel;

    private int red;
    private int green;

    private final SensorEventListener eventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (binding != null) {
                if (event.values != null && event.values.length > 0) {
                    if (event.values[0] < event.sensor.getMaximumRange()) {
                        binding.value.setText(R.string.covered);
                        binding.value.setTextColor(green);
                    } else {
                        binding.value.setText(R.string.uncover);
                        binding.value.setTextColor(red);
                    }
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public ProximitySettingFragment() {
    }

    public static ProximitySettingFragment newInstance() {
        ProximitySettingFragment fragment = new ProximitySettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = getActivity();
        if (activity != null) {
            sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
            proSen = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        }
        red = getResources().getColor(R.color.red1);
        green = getResources().getColor(R.color.green2);
        viewModel = new ViewModelProvider(this).get(SettingViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_proximity_setting, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (sensorManager != null && proSen != null) {
            sensorManager.registerListener(eventListener, proSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }

        if (viewModel.getProType() == Const.PRO_UNLOCK_COVER) {
            switch (viewModel.getProCoverCount()) {
                case 1: {
                    binding.cover1.setChecked(true);
                    break;
                }
                case 2: {
                    binding.cover2.setChecked(true);
                    break;
                }
            }
        } else {
            if (viewModel.getProType() == Const.PRO_UNLOCK_KEEP) {
                switch (viewModel.getProKeepTime()) {
                    case 1: {
                        binding.keepFor1.setChecked(true);
                        break;
                    }
                    case 2: {
                        binding.keepFor2.setChecked(true);
                        break;
                    }
                }
            }
        }

        binding.saveBtn.setOnClickListener(v -> {
            if (binding.cover1.isChecked()) {
                showSavedMsg(viewModel.saveProSettingCover(1));
                return;
            }
            if (binding.cover2.isChecked()) {
                showSavedMsg(viewModel.saveProSettingCover(2));
                return;
            }
            if (binding.keepFor1.isChecked()) {
                showSavedMsg(viewModel.saveProSettingKeep(1));
                return;
            }
            if (binding.keepFor2.isChecked()) {
                showSavedMsg(viewModel.saveProSettingKeep(2));
            }

        });
    }

    private void showSavedMsg(boolean success) {
        if (success) {
            Helper.showToast("success", getContext());
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).notifyServiceUnlockMethodChanged();
            }
        } else {
            Helper.showToast("failed", getContext());
        }
    }
}