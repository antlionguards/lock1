package com.gg.lock1;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.gg.lock1.databinding.ActivityMainBinding;
import com.gg.lock1.other.Const;
import com.gg.lock1.other.Helper;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "TestLock";

    private ActivityMainBinding binding;

    private Messenger messenger;
    private boolean isBound;

    private boolean isUsageStatPermissionGranted = false;
    private boolean isOverlayPermissionGranted = false;

    private final ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            messenger = new Messenger(service);
            isBound = true;
            notifyServiceStateToFragment();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            messenger = null;
            isBound = false;
            notifyServiceStateToFragment();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        if (AppCheckingService.isRunning) {
            Intent intent = new Intent(this, AppCheckingService.class);
            bindService(intent, connection, Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onResume() {
        isUsageStatPermissionGranted = Helper.isUsageStatPermissionGranted(this);
        isOverlayPermissionGranted = Helper.isAppOverlayPermissionGranted(this);

        if (isOverlayPermissionGranted && isUsageStatPermissionGranted) {
            showHomeScreen();
        } else {
            showPermissionWarning();
        }

        super.onResume();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (isBound) {
            unbindService(connection);
            isBound = false;
        }
    }

    public void showPermissionWarning() {
        Fragment topFragment = getSupportFragmentManager().findFragmentById(R.id.mainActivityContainer);
        if (topFragment instanceof PermissionWarningFragment) {
            PermissionWarningFragment warningFragment = (PermissionWarningFragment) topFragment;
            warningFragment.usagePermissionState(isUsageStatPermissionGranted);
            warningFragment.overlayPermissionState(isOverlayPermissionGranted);
        } else {
            FragmentManager fm = getSupportFragmentManager();
            int count = fm.getBackStackEntryCount();
            for (int i = 0; i < count; i++) {
                getSupportFragmentManager().popBackStack();
            }
            replaceFragment(PermissionWarningFragment.newInstance(isUsageStatPermissionGranted, isOverlayPermissionGranted));
        }
    }

    public void showHomeScreen() {
        FragmentManager fm = getSupportFragmentManager();
        Fragment topFragment = fm.findFragmentById(R.id.mainActivityContainer);

        if (topFragment instanceof PermissionWarningFragment || fm.getBackStackEntryCount() == 0) {
            getSupportFragmentManager().popBackStack();
            replaceFragment(HomeFragment.newInstance(isBound));
        }
    }

    public void startAppCheckingService() {
        Intent intent = new Intent(this, AppCheckingService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(intent);
        } else {
            startService(intent);
        }
        new Handler().post(() -> {
            bindService(intent, connection, Context.BIND_AUTO_CREATE);
        });

    }

    public void stopAppCheckingService() {
        if (isBound) {
            unbindService(connection);
            isBound = false;
        }
        stopService(new Intent(this, AppCheckingService.class));
        notifyServiceStateToFragment();
    }

    public void addFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.mainActivityContainer, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    public void replaceFragment(@NonNull Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.mainActivityContainer, fragment)
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    public void openUsagePermissionSetting() {
        Intent setting = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS,
                Uri.parse("package:" + getApplicationContext().getPackageName()));
        startActivity(setting);
    }

    public void openOverlayPermissionSetting() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + getApplicationContext().getPackageName()));
            startActivity(intent);
        }
    }

    public void showProgress() {
        binding.progressLayout.setVisibility(View.VISIBLE);
    }

    public void hideProgress() {
        binding.progressLayout.setVisibility(View.GONE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void notifyServiceToGetBlockedApps() {
        Message msg = Message.obtain(null, Const.MSG_BLOCKED_LIST_CHANGED);
        try {
            if (messenger != null) {
                messenger.send(msg);
            }
        } catch (Exception ignored) {
            Log.d(TAG, "notifyServiceToGetBlockedApps: send msg failed " + ignored);
        }
    }

    public void notifyServiceUnlockMethodChanged() {
        Message msg = Message.obtain(null, Const.MSG_UNLOCK_METHOD_CHANGED);
        try {
            if (messenger != null) {
                messenger.send(msg);
            }
        } catch (Exception ignored) {
            Log.d(TAG, "notifyServiceUnlockMethodChanged: send msg failed " + ignored);
        }
    }

    private void notifyServiceStateToFragment() {
        Fragment topFragment = getSupportFragmentManager().findFragmentById(R.id.mainActivityContainer);
        if (topFragment instanceof HomeFragment) {
            ((HomeFragment) topFragment).handleServiceState(isBound);
        }
    }
}