package com.gg.lock1.other;

import static com.gg.lock1.other.Const.LOCK_MODE_LIGHT;
import static com.gg.lock1.other.Const.LOCK_MODE_MAGNETIC;
import static com.gg.lock1.other.Const.LOCK_MODE_PROXIMITY;
import static com.gg.lock1.other.Const.LOCK_MODE_ROTATE;

public enum LockMethod {
    light(LOCK_MODE_LIGHT),
    magnetic(LOCK_MODE_MAGNETIC),
    pro(LOCK_MODE_PROXIMITY),
    rotate(LOCK_MODE_ROTATE);


    public String value;
    LockMethod(String key){
        value = key;
    }

}