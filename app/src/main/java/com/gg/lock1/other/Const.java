package com.gg.lock1.other;

public class Const {

    public static final String SETTING_SHARED_PREF_NAME = "settingprefs";

    public static final int MSG_BLOCKED_LIST_CHANGED = 1;
    public static final int MSG_UNLOCK_METHOD_CHANGED = 2;

    public static final String LOCK_MODE_LIGHT = "light";
    public static final String LOCK_MODE_MAGNETIC = "mag";
    public static final String LOCK_MODE_PROXIMITY = "proximity";
    public static final String LOCK_MODE_ROTATE = "rotate";


    public static final String LIGHT_UNLOCK_VALUE_START = "light_value_start";
    public static final String LIGHT_UNLOCK_VALUE_END = "light_value_end";
    public static final String LIGHT_UNLOCK_TIME = "light_time";

    public static final String MAG_UNLOCK_VALUE_START = "mag_value_start";
    public static final String MAG_UNLOCK_VALUE_END = "mag_value_end";
    public static final String MAG_UNLOCK_TIME = "mag_time";

    public static final String PRO_UNLOCK_TYPE = "pro_unlock";
    public static final int PRO_UNLOCK_COVER = 1;
    public static final String PRO_UNLOCK_COVER_COUNT = "pro_count_time";
    public static final int PRO_UNLOCK_KEEP = 2;
    public static final String PRO_UNLOCK_KEEP_TIME = "pro_keep_time";

    public static final String ROTATE_UNLOCK_TYPE = "rotate_unlock";
    public static final int ROTATE_UNLOCK_UP = 1;
    public static final int ROTATE_UNLOCK_DOWN = 2;
}
