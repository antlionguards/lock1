package com.gg.lock1.other;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;
import com.airbnb.epoxy.EpoxyDataBindingPattern;
import com.gg.lock1.R;

//@EpoxyDataBindingPattern(rClass = R.class, layoutPrefix = "item_")
@EpoxyDataBindingLayouts({R.layout.item_app})
public interface EpoxyConfig {
}
