package com.gg.lock1.other;

import android.app.AppOpsManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.BindingAdapter;

import static com.gg.lock1.MainActivity.TAG;

import com.gg.lock1.data.UsedAppInfo;

import java.text.DecimalFormat;

public class Helper {

    private static final String PATTERN_HOME_RECENT = "aadd";
    private static final String PATTERN_APP_FIRST_TIME = "daaa";
    private static final String PATTERN_APP_FIRST_TIME_1 = "aada";
    private static final String PATTERN_APP_OPENED = "ddaa";
    private static final String PATTERN_APP = "a";
    private static final String PATTERN_DEFAULT = "d";

    private static final DecimalFormat double3Format = new DecimalFormat("#.###");

    public static String getPattern(@NonNull UsedAppInfo[] usedApps, @NonNull String defaultName) {

        StringBuilder pattern = new StringBuilder();
        for (UsedAppInfo usedApp : usedApps) {
            if (usedApp == null) return "";
            if (defaultName.equals(usedApp.packageName)) {
                pattern.append(PATTERN_DEFAULT);
            } else {
                pattern.append(PATTERN_APP);
            }
        }
        return pattern.toString();
    }

    /**
     * @param pattern -> app pattern from {{@link #getPattern(UsedAppInfo[], String)}}
     * @return position of opened app or -1 if it is not open an app
     */
    public static int isOpenAnApp(String pattern) {
        if (pattern.equals(PATTERN_APP_FIRST_TIME)) {
            return 1;
        }
        if (pattern.equals(PATTERN_APP_FIRST_TIME_1)) {
            return 0;
        }
        if (pattern.equals(PATTERN_APP_OPENED)) {
            return 2;
        }
        return -1;
    }

    public static boolean isPressHomeOrRecent(String pattern) {
        return pattern.equals(PATTERN_HOME_RECENT);
    }

    @BindingAdapter("imageUri")
    public static void setImageUri(ImageView imageView, String uri) {

    }

    @BindingAdapter("imageBitmap")
    public static void setImageBitmap(ImageView imageView, Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @BindingAdapter("imageIconInt")
    public static void setImageIconInt(ImageView imageView, int icon) {
        imageView.setImageResource(icon);
    }

    public static void showToast(String msg, Context context) {
        if (context != null) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
        }
    }

    //clone from epoxy lib
    public static long hashString64Bit(@Nullable CharSequence str) {
        if (str == null) {
            return 0;
        }

        long result = 0xcbf29ce484222325L;
        final int len = str.length();
        for (int i = 0; i < len; i++) {
            result ^= str.charAt(i);
            result *= 0x100000001b3L;
        }
        return result;
    }

    public static boolean isUsageStatPermissionGranted(@NonNull Context context) {
        PackageManager packageManager = context.getPackageManager();
        try {
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getApplicationContext().getPackageName(), 0);
            AppOpsManager appOpsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
            int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                    applicationInfo.uid, applicationInfo.packageName);
            return (mode == AppOpsManager.MODE_ALLOWED);
        } catch (PackageManager.NameNotFoundException e) {
            Log.d(TAG, "isUsagePermissionGranted: got NameNotFoundException" + e);
            return false;
        }
    }


    public static boolean isAppOverlayPermissionGranted(@NonNull Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return Settings.canDrawOverlays(context);
        }
        return true;
    }

    public static String formatDouble3(double value) {
        return double3Format.format(value);
    }

    @Nullable
    public static SharedPreferences getSettingPref(Context context) {
        if (context == null) return null;
        return context.getSharedPreferences(Const.SETTING_SHARED_PREF_NAME, Context.MODE_PRIVATE);
    }

    public static boolean getPrefBoolean(SharedPreferences preferences, String key) {
        if (preferences != null && key != null) {
            return preferences.getBoolean(key, false);
        }
        return false;
    }

    public static boolean putPrefBoolean(SharedPreferences preferences, String key, boolean value) {
        if (preferences != null && key != null) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean(key, value);
            return editor.commit();
        }
        return false;
    }

    public static int getPrefInt(SharedPreferences preferences, String key) {
        if (preferences != null && key != null) {
            return preferences.getInt(key, 0);
        }
        return 0;
    }

}
