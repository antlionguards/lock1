package com.gg.lock1.other;

import com.gg.lock1.data.ShortAppInfo;

public interface ClickCallback {
    default void onCheckedChange(ShortAppInfo appInfo, boolean isChecked) {

    }
}
