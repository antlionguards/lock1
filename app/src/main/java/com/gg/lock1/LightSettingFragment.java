package com.gg.lock1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import com.gg.lock1.databinding.FragmentLightSettingBinding;
import com.gg.lock1.other.Helper;

import java.util.Objects;

public class LightSettingFragment extends Fragment {

    private FragmentLightSettingBinding binding;
    private Sensor lightSen;
    private SensorManager sensorManager;
    private SettingViewModel viewModel;


    private final SensorEventListener eventListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (binding != null) {
                if (event.values != null && event.values.length > 0) {
                    String valueText = "" + event.values[0];
                    binding.value.setText(valueText);
                }
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    };

    public LightSettingFragment() {

    }

    public static LightSettingFragment newInstance() {
        LightSettingFragment fragment = new LightSettingFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity activity = getActivity();
        if (activity != null) {
            sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
            lightSen = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        }
        viewModel = new ViewModelProvider(this).get(SettingViewModel.class);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_light_setting, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.unlockValueEditFrom.setText(String.valueOf(viewModel.getLightValueStart()));
        binding.unlockValueEditTo.setText(String.valueOf(viewModel.getLightValueEnd()));

        if (sensorManager != null && lightSen != null) {
            sensorManager.registerListener(eventListener, lightSen, SensorManager.SENSOR_DELAY_NORMAL, 0);
        }

        binding.timeSeek.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                binding.timeText.setText(getString(R.string.unlock_by_keep_the_above_value_format, progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        binding.timeSeek.setProgress(viewModel.getLightTime());

        binding.saveBtn.setOnClickListener(v -> saveSetting());
    }

    private void saveSetting() {
        String unlockValueStartText = binding.unlockValueEditFrom.getText().toString().trim();
        String unlockValueEndText = binding.unlockValueEditTo.getText().toString().trim();
        try {
            int unlockValueStart = Integer.parseInt(unlockValueStartText);
            int unlockValueEnd = Integer.parseInt(unlockValueEndText);

            if (unlockValueStart > unlockValueEnd) {
                unlockValueStart = unlockValueStart + unlockValueEnd;
                unlockValueEnd = unlockValueStart - unlockValueEnd;
                unlockValueStart = unlockValueStart - unlockValueEnd;
            }
            int time = binding.timeSeek.getProgress();
            if (viewModel.saveLightSetting(unlockValueStart, unlockValueEnd, time)) {
                Helper.showToast("success", getContext());
                Activity activity = getActivity();
                if (activity instanceof MainActivity) {
                    ((MainActivity) activity).notifyServiceUnlockMethodChanged();
                }
            } else {
                Helper.showToast("failed", getContext());
            }
        } catch (Exception ignored) {
        }
    }
}