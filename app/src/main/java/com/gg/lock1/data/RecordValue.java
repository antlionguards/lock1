package com.gg.lock1.data;

public class RecordValue {
    public float value;
    public long timeMillis;

    public RecordValue(float value, long timeMillis) {
        this.value = value;
        this.timeMillis = timeMillis;
    }

    public RecordValue() {
    }
}
