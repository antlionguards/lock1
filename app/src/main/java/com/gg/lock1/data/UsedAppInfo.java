package com.gg.lock1.data;

public class UsedAppInfo {
    public String packageName = "";
    public long timeSinceLastVisible;

    public UsedAppInfo() {
    }

    public UsedAppInfo(String packageName, long timeSinceLastVisible) {
        this.packageName = packageName;
        this.timeSinceLastVisible = timeSinceLastVisible;
    }
}
