package com.gg.lock1.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class ShortAppInfo {
    private CharSequence name;
    private Bitmap bitmap;
    private CharSequence packageName;

    private boolean isBlocked = false;

    public CharSequence getName() {
        return name;
    }

    public void setName(CharSequence name) {
        this.name = name;
    }

    public CharSequence getPackageName() {
        return packageName;
    }

    public void setPackageName(CharSequence packageName) {
        this.packageName = packageName;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }
}
