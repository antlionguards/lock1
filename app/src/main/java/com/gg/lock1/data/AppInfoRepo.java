package com.gg.lock1.data;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Log;

import static com.gg.lock1.MainActivity.TAG;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gg.lock1.other.Helper;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.Single;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.core.SingleOnSubscribe;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AppInfoRepo {

    private final Box<BlockedAppInfo> blockedAppInfoBox = ObjectBox.get().boxFor(BlockedAppInfo.class);

    public Single<List<ShortAppInfo>> getInstalledApps(@NonNull PackageManager packageManager) {
        return Single.create((SingleOnSubscribe<List<ShortAppInfo>>) emitter -> {
            long startMeasure = System.currentTimeMillis();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_LAUNCHER);

            List<ResolveInfo> infoList = packageManager.queryIntentActivities(intent, PackageManager.GET_META_DATA);
            List<BlockedAppInfo> blockedApps = blockedAppInfoBox.getAll();

            List<ShortAppInfo> shortAppInfoList = new ArrayList<>(infoList.size());
            ShortAppInfo appItem;
            Drawable drawable;
            Canvas canvas = new Canvas();
            Bitmap bitmap;
            int i;
            String blockedAppPackageName;

            for (ResolveInfo info : infoList) {
                appItem = new ShortAppInfo();
                appItem.setName(info.loadLabel(packageManager));
                appItem.setPackageName(info.activityInfo.packageName);

                for (i = 0; i < blockedApps.size(); i++) {
                    blockedAppPackageName = blockedApps.get(i).getPackageName();
                    if (appItem.getPackageName() != null && appItem.getPackageName().equals(blockedAppPackageName)) {
                        appItem.setBlocked(true);
                        Log.d(TAG, "getInstalledApps: got a blocked = " + appItem.getPackageName() + "/" +blockedApps.size());
                        blockedApps.remove(i);
                        break;
                    }
                }

                drawable = info.loadIcon(packageManager);
                bitmap = Bitmap.createBitmap(96, 96, Bitmap.Config.ARGB_8888);
                drawable.setBounds(0, 0, 96, 96);

                canvas.setBitmap(bitmap);
                drawable.draw(canvas);

                appItem.setBitmap(bitmap);
                //Log.d(TAG, "getInstalledApps: " + appItem.getName());
                shortAppInfoList.add(appItem);
            }
            emitter.onSuccess(shortAppInfoList);
            Log.d(TAG, "\n" + "____\ngetInstalledApps end: " + (System.currentTimeMillis() - startMeasure) + "\n____");
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @NonNull
    private <T> LiveData<T> basicNullError(@NonNull SingleOnSubscribe<T> source) {
        MutableLiveData<T> liveData = new MutableLiveData<>();
        Single
                .create(source)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<T>() {
                    @Override
                    public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {
                    }

                    @Override
                    public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull T t) {
                        liveData.setValue(t);
                    }

                    @Override
                    public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                        liveData.setValue(null);
                    }
                });
        return liveData;
    }

    public LiveData<List<BlockedAppInfo>> getBlockedApps() {
        return basicNullError(emitter -> {
            List<BlockedAppInfo> blockedApps = blockedAppInfoBox.getAll();
            emitter.onSuccess(blockedApps);
        });
    }

    public LiveData<Boolean> blockApp(ShortAppInfo appInfo) {
        return basicNullError(emitter -> {
            BlockedAppInfo blockedAppInfo = new BlockedAppInfo();
            blockedAppInfo.setId(Helper.hashString64Bit(appInfo.getPackageName()));
            blockedAppInfo.setPackageName(String.valueOf(appInfo.getPackageName()));
            blockedAppInfoBox.put(blockedAppInfo);
            emitter.onSuccess(true);
        });
    }

    public LiveData<Boolean> removeBlockApp(ShortAppInfo appInfo) {
        return basicNullError(emitter -> {
            BlockedAppInfo blockedAppInfo = new BlockedAppInfo();
            blockedAppInfo.setId(Helper.hashString64Bit(appInfo.getPackageName()));
            blockedAppInfo.setPackageName(String.valueOf(appInfo.getPackageName()));
            blockedAppInfoBox.remove(blockedAppInfo);
            emitter.onSuccess(true);
        });
    }


}
