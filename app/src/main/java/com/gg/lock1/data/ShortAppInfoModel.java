package com.gg.lock1.data;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.airbnb.epoxy.EpoxyAttribute;
import com.airbnb.epoxy.EpoxyHolder;
import com.airbnb.epoxy.EpoxyModelClass;
import com.airbnb.epoxy.EpoxyModelWithHolder;
import com.gg.lock1.R;
import com.gg.lock1.databinding.ItemAppBinding;

@EpoxyModelClass(layout = R.layout.item_app)
abstract class ShortAppInfoModel extends EpoxyModelWithHolder<ShortAppInfoModel.Holder> {

    @EpoxyAttribute
    ShortAppInfo appInfo;

    @Override
    protected int getDefaultLayout() {
        return R.layout.item_app;
    }

    @Override
    public void bind(@NonNull Holder holder) {
        if (appInfo != null) {
            holder.binding.setAppInfo(appInfo);
        }
    }

    static class Holder extends EpoxyHolder {
        ItemAppBinding binding;

        @Override
        protected void bindView(@NonNull View itemView) {
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
