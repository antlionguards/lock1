package com.gg.lock1.data;

import java.util.Objects;

import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;

@Entity
public class BlockedAppInfo {
    @Id(assignable = true)
    private long id;

    private String packageName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockedAppInfo that = (BlockedAppInfo) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, packageName);
    }
}
