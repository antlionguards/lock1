package com.gg.lock1;

import android.app.Activity;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.lock1.databinding.FragmentSettingBinding;
import com.gg.lock1.other.LockMethod;

public class SettingFragment extends Fragment {

    private FragmentSettingBinding binding;

    private SettingViewModel settingViewModel;

    private boolean hasLightSen;
    private boolean hasMagSen;
    private boolean hasProSen;
    private boolean hasRotateSen;

    public SettingFragment() {
        // Required empty public constructor
    }

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        settingViewModel = new ViewModelProvider(this).get(SettingViewModel.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_setting, container, false);
        return binding.getRoot();
    }

    void getSensors() {
        Activity activity = getActivity();
        if (activity != null) {
            SensorManager sensorManager = (SensorManager) activity.getSystemService(Context.SENSOR_SERVICE);
            hasLightSen = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null;
            hasMagSen = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null;
            hasProSen = sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null;
            hasRotateSen = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null;
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getSensors();
        addActions();
        getSetting();
    }

    void getSetting() {
        binding.lightSwitch.setChecked(settingViewModel.getLockModeStatus(LockMethod.light.value));
        binding.magneticSwitch.setChecked(settingViewModel.getLockModeStatus(LockMethod.magnetic.value));
        binding.proximitySwitch.setChecked(settingViewModel.getLockModeStatus(LockMethod.pro.value));
        binding.rotateSwitch.setChecked(settingViewModel.getLockModeStatus(LockMethod.rotate.value));

        binding.lightSwitch.setEnabled(hasLightSen);
        binding.magneticSwitch.setEnabled(hasMagSen);
        binding.proximitySwitch.setEnabled(hasProSen);
        binding.rotateSwitch.setEnabled(hasRotateSen);

        binding.lightSetting.setEnabled(hasLightSen);
        binding.magneticSetting.setEnabled(hasMagSen);
        binding.proximitySetting.setEnabled(hasProSen);
        binding.rotateSetting.setEnabled(hasRotateSen);

        binding.lightAvailability.setText(settingViewModel.getStatusString(hasLightSen));
        binding.magneticAvailability.setText(settingViewModel.getStatusString(hasMagSen));
        binding.proximityAvailability.setText(settingViewModel.getStatusString(hasProSen));
        binding.rotateAvailability.setText(settingViewModel.getStatusString(hasRotateSen));
    }

    void addActions() {
        binding.lightSwitch.setOnClickListener(v -> {
            boolean isEnabled = settingViewModel.toggleLockMode(LockMethod.light.value);
            notifyToService();
            binding.lightSwitch.setChecked(isEnabled);
        });

        binding.magneticSwitch.setOnClickListener(v -> {
            boolean isEnabled = settingViewModel.toggleLockMode(LockMethod.magnetic.value);
            notifyToService();
            binding.magneticSwitch.setChecked(isEnabled);
        });

        binding.proximitySwitch.setOnClickListener(v -> {
            boolean isEnabled = settingViewModel.toggleLockMode(LockMethod.pro.value);
            notifyToService();
            binding.proximitySwitch.setChecked(isEnabled);
        });

        binding.rotateSwitch.setOnClickListener(v -> {
            boolean isEnabled = settingViewModel.toggleLockMode(LockMethod.rotate.value);
            notifyToService();
            binding.rotateSwitch.setChecked(isEnabled);
        });

        binding.lightSetting.setOnClickListener(v -> openSubSetting(LightSettingFragment.newInstance()));
        binding.magneticSetting.setOnClickListener(v -> openSubSetting(MagSettingFragment.newInstance()));
        binding.proximitySetting.setOnClickListener(v -> openSubSetting(ProximitySettingFragment.newInstance()));
        binding.rotateSetting.setOnClickListener(v -> openSubSetting(RotateSettingFragment.newInstance()));
    }

    private void openSubSetting(Fragment fragment){
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).replaceFragment(fragment);
        }
    }

    private void notifyToService(){
        Activity activity = getActivity();
        if (activity instanceof MainActivity) {
            ((MainActivity) activity).notifyServiceUnlockMethodChanged();
        }
    }
}