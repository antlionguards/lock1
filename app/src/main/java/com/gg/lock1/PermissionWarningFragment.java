package com.gg.lock1;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gg.lock1.databinding.FragmentPermissionWarningBinding;

public class PermissionWarningFragment extends Fragment {

    private FragmentPermissionWarningBinding binding;

    private final String KEY_USAGE_PERMISSION_STATUS = "usage_permission_status";
    private final String KEY_OVERLAY_PERMISSION_STATUS = "overlay_permission_status";
    private boolean isUsageStatPermissionGranted = false;
    private boolean isOverlayPermissionGranted = false;

    public PermissionWarningFragment() {
        // Required empty public constructor
    }

    public static PermissionWarningFragment newInstance(boolean isUsageStatPermissionGranted, boolean isOverlayPermissionGranted) {
        PermissionWarningFragment fragment = new PermissionWarningFragment();
        Bundle args = new Bundle();
        args.putBoolean(fragment.KEY_USAGE_PERMISSION_STATUS, isUsageStatPermissionGranted);
        args.putBoolean(fragment.KEY_OVERLAY_PERMISSION_STATUS, isOverlayPermissionGranted);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args;
        if (savedInstanceState != null) {
            args = savedInstanceState;
        } else {
            args = getArguments();
        }
        if (args != null) {
            isUsageStatPermissionGranted = args.getBoolean(KEY_USAGE_PERMISSION_STATUS, false);
            isOverlayPermissionGranted = args.getBoolean(KEY_OVERLAY_PERMISSION_STATUS, false);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putBoolean(KEY_USAGE_PERMISSION_STATUS, isUsageStatPermissionGranted);
        outState.putBoolean(KEY_OVERLAY_PERMISSION_STATUS, isOverlayPermissionGranted);
        super.onSaveInstanceState(outState);
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_permission_warning, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.overlayBtn.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openOverlayPermissionSetting();
            }
        });

        binding.usageBtn.setOnClickListener(v -> {
            Activity activity = getActivity();
            if (activity instanceof MainActivity) {
                ((MainActivity) activity).openUsagePermissionSetting();
            }
        });

        usagePermissionState(isUsageStatPermissionGranted);
        overlayPermissionState(isOverlayPermissionGranted);
    }

    public void usagePermissionState(boolean isEnabled) {
        isUsageStatPermissionGranted = isEnabled;
        if (isEnabled) {
            binding.usageBtn.setVisibility(View.GONE);
            if (getActivity() != null) {
                Drawable drawable = ResourcesCompat.getDrawable(getResources(),
                        R.drawable.ic_baseline_check_circle_24, getActivity().getTheme());
                binding.usageTitle.setCompoundDrawables(null, null, drawable, null);
            }
        } else {
            binding.usageBtn.setVisibility(View.VISIBLE);
            binding.usageTitle.setCompoundDrawables(null, null, null, null);
        }
    }

    public void overlayPermissionState(boolean isEnabled) {
        isOverlayPermissionGranted = isEnabled;
        if (isEnabled) {
            binding.overlayBtn.setVisibility(View.GONE);
            if (getActivity() != null) {
                Drawable drawable = ResourcesCompat.getDrawable(getResources(),
                        R.drawable.ic_baseline_check_circle_24, getActivity().getTheme());
                binding.overlayTitle.setCompoundDrawables(null, null, drawable, null);
            }
        } else {
            binding.overlayBtn.setVisibility(View.VISIBLE);
            binding.overlayTitle.setCompoundDrawables(null, null, null, null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}